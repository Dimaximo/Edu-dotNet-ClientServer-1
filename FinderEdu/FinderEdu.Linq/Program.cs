﻿using System;

namespace FinderEdu.Linq
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("[LINQ DEMO]");

            var demo = new Demo();
            demo.Show();

            Console.WriteLine("[DONE]");
            Console.ReadKey();
        }
    }
}
