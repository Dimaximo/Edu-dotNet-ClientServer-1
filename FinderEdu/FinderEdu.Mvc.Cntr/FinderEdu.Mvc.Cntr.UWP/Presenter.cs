﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinderEdu.Mvc.Cntr;

namespace FinderEdu.Mvc.Cntr.UWP
{
    public class Presenter
    {
        private readonly ICounterPage _view;
        private readonly Counter _counter;

        public Presenter(ICounterPage view)
        {
            _counter = new Counter(0, 10);
            _view = view;
            _view.CountRequested += OnCountRequested;
        }

        private void OnCountRequested(object sender, EventArgs e)
        {
            if (_counter.Count()) _view.SetCounts(_counter.Value);
        }
    }

}
