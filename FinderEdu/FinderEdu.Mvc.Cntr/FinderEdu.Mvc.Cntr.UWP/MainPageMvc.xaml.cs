﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using FinderEdu.Mvc.Cntr;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace FinderEdu.Mvc.Cntr.UWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPageMvc : Page
    {
        Counter counter = new Counter(0, 10);

        public MainPageMvc()
        {
            this.InitializeComponent();
        }

        private void CntrButtonClick(object sender, RoutedEventArgs e)
        {
            if (counter.Count()) cntrButton.Content = $"Clicks : {counter.Value}";
        }
    }
}