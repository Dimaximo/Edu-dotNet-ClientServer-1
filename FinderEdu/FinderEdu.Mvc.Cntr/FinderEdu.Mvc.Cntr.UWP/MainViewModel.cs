﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinderEdu.Mvc.Cntr;

namespace FinderEdu.Mvc.Cntr.UWP
{
    public class MainViewModel : ObservableObject
    {
        private readonly FinderEdu.Mvc.Cntr.Counter _counter;
        private string _format;

        public int Counts => _counter.Value;

        public bool IsReachedUp => _counter.IsReachedUp;

        public string FormatString
        {
            get { return _format; }
            set { if (SetProperty(ref _format, value)) Update(); }
        }

        public string CountsString
        {
            get
            {
                if (string.IsNullOrWhiteSpace(FormatString))
                {
                    return Counts.ToString();
                }
                else
                {
                    return string.Format(FormatString, Counts);
                }
            }
        }

        public Command CountCommand { get; private set; }

        public bool CanCountMore => !IsReachedUp;

        public MainViewModel()
        {
            CountCommand = new Command(Count, () => CanCountMore);
            _counter = new Counter(0, 10);
            _counter.ValueChanged += (s, ea) => Update();
        }

        private void Update()
        {
            CountCommand.RaiseCanExecuteChanged();
            OnAllPropertiesChanged();
        }

        public void Count()
        {
            _counter.Count();
        }
    }
}
