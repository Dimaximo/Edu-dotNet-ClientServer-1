﻿using System;

namespace FinderEdu.Mvc.Cntr
{
    public class Counter
    {
        public event EventHandler ValueChanged;

        private int _value;

        public int Value
        {
            get { return _value; }
            private set
            {
                if (value != _value)
                {
                    _value = value;
                    ValueChanged?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        public int MaxValue { get; private set; }

        public bool IsReachedUp => Value >= MaxValue;

        public Counter(int initialValue, int maxValue)
        {
            if (initialValue > maxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(initialValue));
            }

            Value = initialValue;
            MaxValue = maxValue;
        }

        public bool Count()
        {
            // concurrency problem here!
            if (IsReachedUp) return false;
            Value++;
            return true;
        }
    }
}

