﻿using FinderEdu.ToDoList.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace FinderEdu.ToDoList.DAL
{
    public class DatabaseContext : DbContext
    {
        public DbSet<ToDoItem> ToDoItems { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
        }

    }
}