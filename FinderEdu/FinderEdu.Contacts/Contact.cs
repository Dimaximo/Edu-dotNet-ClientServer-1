﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinderEdu.Contacts
{
    public class Contact
    {
        string _name;
        //[JsonProperty("ContactName")]
        public string  Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        //[JsonProperty("ContactPhone")]
        public string  Phone { get; set; }

        public Contact()
        {

        }

        public Contact(string name, string phone)
        {
            Name = name;
            Phone = phone;
        }

        public override string ToString()
        {
            return String.Format("{0,16}[{1}]", Name, Phone);
        }
    }
}
