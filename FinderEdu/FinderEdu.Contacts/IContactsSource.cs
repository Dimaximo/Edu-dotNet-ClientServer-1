﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinderEdu.Contacts
{
    public interface IContactsSource
    {
        Contact[] GetContacts();
        
        
        //IEnumerable<Contact> GetContacts();
    }
}
