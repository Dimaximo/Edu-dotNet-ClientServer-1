﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Xml.Serialization;

namespace FinderEdu.Contacts
{
    class Program
    {
        static void Main(string[] args)
        {
            Scenario1();
            Scenario2();
        }

        /// <summary>
        /// Show polymorphism
        /// </summary>
        static void Scenario1()
        {
            var sources = new IContactsSource[] 
            {
                new ApplicationContacts(),
                new Google(),
                new DeviceContacts()
            };

            Console.WriteLine("polymorphism demo.");
            Console.WriteLine("Let's print contacs from all accessable sources!");
            Console.WriteLine("CONTACTS :");

            for (int i = 0; i<sources.Length; i++)
            {
                var contacts = sources[i].GetContacts();
                foreach (Contact contact in contacts)
                {
                    Console.WriteLine(contact);
                }
            }

            Console.WriteLine("-------------");
            Console.WriteLine("done.");
            Console.ReadKey();
        }

        /// <summary>
        /// Show json and attributes
        /// </summary>
        static void Scenario2()
        {
            var sources = new IContactsSource[]
            {
                new ApplicationContacts(),
                new Google(),
                new DeviceContacts()
            };

            Console.WriteLine("serialization demo.");
            Console.WriteLine("Let's print serialized data from all accessable sources!");
            
            // LINQ usage
            var allContacts = sources.SelectMany(source => source.GetContacts());

            // Json
            Console.WriteLine("JSON :");
            var json = JsonConvert.SerializeObject(allContacts,Formatting.Indented);
            Console.WriteLine(json);

            Console.WriteLine("-------------");
            Console.WriteLine("done.");
            Console.ReadKey();
        }
    }
}
