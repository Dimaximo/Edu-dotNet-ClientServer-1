﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FinderEdu.Delegates.Tpl
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            SubscribeToEmptyBox();
        }

        #region EmptyTextBox events
        private void SubscribeToEmptyBox()
        {
            // 9.1. subscribe to EmptyChanged1
            emptyBox.EmptyChanged1 += NotifyIfEmpty;

            // 9.3 Can't reassign event
            //emptyBox.EmptyChanged2 = NotifyIfEmpty;

            // 9.4 unsubscribe from EmptyChanged3
            emptyBox.EmptyChanged3 -= NotifyIfEmptyDel;

            // 9.5 We can manipulate as we want! (Anarchy!)
            emptyBox.EmptyChangedD += NotifyIfEmptyDel;
            emptyBox.EmptyChangedD = NotifyIfEmptyDel;
            emptyBox.EmptyChangedD = null;
        }

        // 9.2.1 First Handler
        private void NotifyIfEmpty(object sender, EmptyChangedEventArgs e)
        {
            if (e.IsEmpty) MessageBox.Show($"{sender} is empty!");
        }


        // 9.2.2 Second handler
        private void NotifyIfEmptyDel(bool empty)
        {
            if (empty) MessageBox.Show("Something is empty!");
        }
        #endregion

        #region Buttons
        private void SyncButtonClick(object sender, RoutedEventArgs e)
        {
            ComputeAvgRndAndMsgBox();
        }

        private void ThreadButtonClick(object sender, RoutedEventArgs e)
        {
            var t = new Thread(ComputeAvgRndAndMsgBox);
            t.Start();
        }

        private void ThreadPoolButtonClick(object sender, RoutedEventArgs e)
        {
            // use lambda, becouse of WaitCallback delegate
            ThreadPool.QueueUserWorkItem((state)=>ComputeAvgRndAndMsgBox(), null);
        }

        private void TaskWaitButtonClick(object sender, RoutedEventArgs e)
        {
            var t = Task.Factory.StartNew(ComputeAvgRndAndMsgBox);
            t.Wait();            
        }

        private void TaskButtonClick(object sender, RoutedEventArgs e)
        {
            var task = new Task<double>(ComputeAvg);
            task.Start();
            double result = task.Result;

            MessageBox.Show(result.ToString());
        }

        private void TaskUiErrorButtonClick(object sender, RoutedEventArgs e)
        {
            var t = Task.Factory.StartNew(()=>
                {
                    var avg = Demo.AvgRandom(300_000_000);
                    resultText.Text = avg.ToString();
                }
            );
        }

        private void TaskAndContextButtonClick(object sender, RoutedEventArgs e)
        {
            var context = SynchronizationContext.Current;
            Task t = Task.Factory.StartNew(() =>
                {
                    // this part in background thread
                    var avg = ComputeAvg();
                    // closure on 'context'
                    context.Post((state)=> resultText.Text = avg.ToString(), null);
                    // resultText update in UI thread
                }
            );
        }

        private async void TaskAndAsyncButtonClick(object sender, RoutedEventArgs e)
        {
            Task<double> t = Task.Factory.StartNew(() =>
                {
                    // this part in background thread
                    var avg = ComputeAvg();
                    return avg;
                }
            );

            double result = await t;
            resultText.Text = result.ToString();
        }

        private async void TaskAndProgressButtonClick(object sender, RoutedEventArgs e)
        {
            // UI : prepare ui
            resultText.Visibility = Visibility.Collapsed;
            busyIndicator.Visibility = Visibility.Visible;
            controlsPanel.IsEnabled = false;

            // BACKGROUND : start background thread and await results
            var t = Task.Factory.StartNew(ComputeAvg);
            double result = await t;

            // UI : apply results in UI thread
            resultText.Text = result.ToString();

            // UI : update ui
            resultText.Visibility = Visibility.Visible;
            busyIndicator.Visibility = Visibility.Collapsed;
            controlsPanel.IsEnabled = true;
        }
        #endregion

        #region COMPUTING

        private void ComputeAvgRndAndMsgBox()
        {
            var avg = Demo.AvgRandom(300_000_000);
            MessageBox.Show(avg.ToString());
        }

        private double ComputeAvg()
        {
            var avg = Demo.AvgRandom(300_000_000);
            return avg;
        }

        #endregion

    }
}
