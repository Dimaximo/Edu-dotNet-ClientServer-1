﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace FinderEdu.Delegates.Tpl
{

    // 6. Define own EventArgs
    public class EmptyChangedEventArgs : EventArgs
    {
        public bool IsEmpty { get; private set; }

        public EmptyChangedEventArgs(bool empty)
        {
            IsEmpty = empty;
        }
    }


    // 5. Define own delegate
    public delegate void EmptyChangedDelegate(bool empty);

    //  1. Define own TextBox class
    public class EmptyTextBox: TextBox
    {
        // 7. Declare events
        public event EventHandler<EmptyChangedEventArgs> EmptyChanged1;
        public event EventHandler EmptyChanged2;
        public event EmptyChangedDelegate EmptyChanged3;

        public EmptyChangedDelegate EmptyChangedD;

        // 2. Define IsEmpty property
        bool _empty;
        public bool IsEmpty => string.IsNullOrEmpty(this.Text);

        public EmptyTextBox()
        {
            _empty = IsEmpty;
        }

        // 3. Override protected internal method OnTextChanged
        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            base.OnTextChanged(e);

            // 4.1 Add logic to handle IsEmpty changes
            if (_empty != IsEmpty)
            {
               _empty = IsEmpty;
                OnEmptyChanged();
            }
        }


        // 4.2 Add logic to handle IsEmpty changes
        private void OnEmptyChanged()
        {
            // 8. Call event handlers
            var handler = EmptyChanged1;
            if (handler != null)
            {
                handler.Invoke(this, new EmptyChangedEventArgs(IsEmpty));
            }

            EmptyChanged2?.Invoke(this, new EmptyChangedEventArgs(IsEmpty));

            EmptyChanged3?.Invoke(IsEmpty);

            if (EmptyChangedD != null) EmptyChangedD(IsEmpty);
        }

    }
}
