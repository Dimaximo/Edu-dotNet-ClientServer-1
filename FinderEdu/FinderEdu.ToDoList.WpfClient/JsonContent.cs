﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace FinderEdu.ToDoList.WpfClient
{
    public class JsonContent : StringContent
    {
        private const string MediaType = "application/json";

        public JsonContent(string json)
            : base(json)
        {
            Headers.ContentType = new MediaTypeHeaderValue(MediaType);
        }

        public static JsonContent FromObject(object obj)
        {
            var content = JsonConvert.SerializeObject(obj);
            return new JsonContent(content);
        }
    }
}
