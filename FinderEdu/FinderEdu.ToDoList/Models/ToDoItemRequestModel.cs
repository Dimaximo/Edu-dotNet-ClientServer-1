﻿using FinderEdu.ToDoList.DAL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FinderEdu.ToDoList.Models
{
    public class ToDoItemRequestModel
    {
        public Guid Id { get; set; }
        [MaxLength(64), Required(AllowEmptyStrings = false)]
        public string Title { get; set; }
        [MaxLength(1024)]
        public string Description { get; set; }

        public ToDoItem ToDbModel(Guid ownerId)
        {
            var now = DateTime.UtcNow;
            return new ToDoItem()
            {
                Id = this.Id,
                OwnerId = ownerId,
                Title = this.Title,
                Description = this.Description,
                CreatedAt = now,
                ModifiedAt = now
            };
        }
    }
}
