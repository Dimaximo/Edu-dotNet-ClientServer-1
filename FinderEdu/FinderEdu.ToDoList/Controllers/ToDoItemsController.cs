﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FinderEdu.ToDoList.DAL.Models;
using FinderEdu.ToDoList.Models;
using FinderEdu.ToDoList.DAL.Repo;
using Microsoft.EntityFrameworkCore;

namespace FinderEdu.Controllers
{
    [Route("api/[controller]")]
    public class ToDoItemsController : Controller
    {
        private readonly IRepo<Guid, ToDoItem> _repo;

        public ToDoItemsController(IRepo<Guid, ToDoItem> repo)
        {
            _repo = repo;
        }

        // GET api/todoitems/userid
        [HttpGet("{userId:guid}")]
        public IActionResult Get(Guid userId)
        {
            var userItems = _repo.GetAll().Where(item=>item.OwnerId == userId);
            return Json(userItems);
        }

        // GET api/todoitems/demo/userid
        [HttpGet("Demo/{userId:guid}")]
        public IActionResult GetDemo(Guid userId)
        {
            return Json(GetDemoItemsForClient(userId));
        }

        // GET api/todoitems/userid/id
        [HttpGet("{userId:guid}/{id:guid}")]
        public async Task<IActionResult> Get(Guid userId, Guid id)
        {
            var item = await _repo.GetAsync(id);
            if (item == null) return NotFound();
            if (item.OwnerId != userId) return Unauthorized();
            return Json(item);
        }

        // PUT api/todoitems/userid/id
        [HttpPut("{userId:guid}")]
        public async Task<IActionResult> Put(Guid userId, [FromBody]ToDoItemRequestModel model)
        {
            if (model == null || userId == Guid.Empty || model.Id == Guid.Empty)
            {
                return BadRequest();
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.ToString());
            }
            var existedItem = await _repo.GetAsync(model.Id);
            var newItem = model.ToDbModel(userId);
            if (existedItem == null)
            {
                // add new item
                await _repo.InsertAsync(newItem);
                await _repo.SaveAsync();
                return CreatedAtAction("Get", new { userId, newItem.Id }, newItem);
            }
            else if (existedItem.OwnerId!=userId)
            {
                // item has another owner
                return Unauthorized();
            }
            else
            {
                // item already in DB
                newItem.CreatedAt = existedItem.CreatedAt;
                _repo.StopTracking(existedItem);
                await _repo.UpdateAsync(newItem);
                await _repo.SaveAsync();
                return NoContent();
            }
        }

        // DELETE api/todoitems/userid/id
        [HttpDelete("{userId:guid}/{id:guid}")]
        public async Task<IActionResult> Delete(Guid userId, Guid id)
        {
            var item = await _repo.GetAsync(id);
            if (item == null) return NotFound();
            if (item.OwnerId != userId) return Unauthorized();
            await _repo.DeleteAsync(id);
            await _repo.SaveAsync();
            return NoContent();
        }

        private static IEnumerable<ToDoItemResponseModel> GetDemoItemsForClient(
            Guid clientId)
        {
            return new[]
            {
            new ToDoItemResponseModel()
            {
                Title = "Buy a milk",
                Description = "We need milk for shakes!",
                Id = Guid.NewGuid(),
                OwnerId = clientId,
                CreatedAt = DateTime.UtcNow,
                ModifiedAt = DateTime.UtcNow
            },
            new ToDoItemResponseModel()
            {
                Title = "Anniversary",
                Description = "Call a mom. 50 years",
                Id = Guid.NewGuid(),
                OwnerId = clientId,
                CreatedAt = DateTime.UtcNow,
                ModifiedAt = DateTime.UtcNow
            },
            new ToDoItemResponseModel()
            {
                Title = "New item (1)",
                Description = String.Empty,
                Id = Guid.NewGuid(),
                OwnerId = clientId,
                CreatedAt = DateTime.UtcNow,
                ModifiedAt = DateTime.UtcNow
            },
            };
        }
    }
}
