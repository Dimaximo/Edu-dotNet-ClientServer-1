﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinderEdu.Delegates.dotNet.Done
{
    public delegate void Processor(int value);

    public static class Iterator
    {
        public static void ForAll(int[] array, Processor processor)
        {
            for(int i = 0; i < array.Length; i++)
            {
                processor(array[i]);
            }
        }
    }
}
