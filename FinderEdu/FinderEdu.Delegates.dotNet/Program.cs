﻿using System;

namespace FinderEdu.Delegates.dotNet
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("[DELEGATES DEMO]");

            var demo = new Done.Demo();
            demo.Show();

            Console.WriteLine("[DONE]");
            Console.ReadKey();
        }
    }
}
