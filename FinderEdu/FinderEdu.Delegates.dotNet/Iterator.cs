﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinderEdu.Delegates.dotNet
{
    public delegate void Processor(int item);

    public class Iterator
    {
        public static string title;

        public static void ForAll(int[] items,
            Processor processor)
        {
            for(int i =0; i < items.Length; i++)
            {
                processor(items[i]);
            }
        }
    }
}
