﻿using System;
using System.Runtime.InteropServices;

namespace FinderEdu.Oop.Complex
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("DEMO 'From struct to OOP' :");
            Console.WriteLine();

            Encapsulation.Demo.Show();
            //DI.Demo.Show();

            Console.ReadKey();
        }

    }
}
