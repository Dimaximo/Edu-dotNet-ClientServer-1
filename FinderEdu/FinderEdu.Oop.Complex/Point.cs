﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinderEdu.Oop.Complex
{
    public interface IPoint
    {
        double X { get; }
        double Y { get; }
    }
    public class Point : IPoint
    {
        double _x;
        public double X
        {
            get { return _x; }
            set { _x = value; }
        }
        public double Y { get; set; }
    }
}
