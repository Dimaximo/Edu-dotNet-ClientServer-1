﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinderEdu.Oop.Complex.DI
{
    public class ClientHistory
    {
        private readonly ISettings _settings;

        // DEP. INJECTION HERE!
        public ClientHistory(ISettings settings)
        {
            _settings = settings;
        }

        public void SaveLastComplex(Complex complex)
        {
            _settings.Save("LastComplexKey", complex);
        }

        public Complex LoadLastComplex()
        {
            return (Complex) _settings.Load("LastComplexKey");
        }
    }
}
