﻿#define step_1
using System;
using System.Collections.Generic;
using System.Text;

namespace FinderEdu.Oop.Complex.DI
{
    public struct Complex : IPoint
    {
        private static Complex FakeInFileValue = new Complex();

        public double Real;
        public double Img;

        public double X => Real;

        public double Y => Img;
        
        public override string ToString()
        {
            return $"{Real} + {Img}i";
        }

        public void SaveToFile(string fileName)
        {
            SaveToFile(this, fileName);
        }

        public static void SaveToFile(
            Complex complex,
            string fileName)
        {
            FakeInFileValue = complex;
        }

        public static Complex ReadFromFile(string fileName)
        {
            return FakeInFileValue;
        }
    }

    public static class ComplexMath
    {
        public static Complex Add(Complex left, Complex right)
        {
            Complex result = new Complex();
            result.Real = left.Real + right.Real;
            result.Img = left.Img + right.Img;
            return result;
        }

        public static Complex Sub(Complex left, Complex right)
        {
            var result = new Complex()
            {
                Real = left.Real - right.Real,
                Img = left.Img - right.Img
            };
            
            return result;
        }

        public static double Abs(this Complex complex)
        {
            return Math.Sqrt(complex.Real * complex.Real + complex.Img * complex.Img);
        }
    }


    public static class Demo
    {
        public static void Show()
        {
            WithoutDI();
            Console.ReadKey();
            WithDI();
        }
               
        private static void WithoutDI()
        {

            Console.WriteLine("[Without DI]");

            Complex c1 = new Complex() { Real = 2, Img = 1 };

            Console.WriteLine($"New c1 = {c1}");

            // WRONG WAY :

            c1.SaveToFile("fake.dat");

            c1.Real = 3;
            c1.Img = 4;
            Console.WriteLine($"After modification c1 = {c1}");

            c1 = Complex.ReadFromFile("fake.dat");
            Console.WriteLine($"After reading from file c1 = {c1}");

            Console.ReadKey();
        }

        private static void WithDI()
        {
            // RIGHT WAY (WITH DI) :

            Console.WriteLine("[With DI]");

            ISettings settings = ServiceLocator.Settings;
            ClientHistory history = new ClientHistory(settings);

            Complex c1 = new Complex() { Real = 2, Img = 1 };
            Console.WriteLine($"Before saving to settings c1 = {c1}");
            history.SaveLastComplex(c1);

            c1.Real = 5;
            c1.Img = 6;
            Console.WriteLine($"After modification c1 = {c1}");

            c1 = history.LoadLastComplex();
            Console.WriteLine($"After reading settings c1 = {c1}");

        }

    }
}
