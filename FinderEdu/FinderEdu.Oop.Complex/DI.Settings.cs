﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinderEdu.Oop.Complex.DI
{
    public interface ISettings
    {
        void Save(string key, object value);
        object Load(string key);
    }

    public class Settings : ISettings
    {
        private Dictionary<string, object> settings
            = new Dictionary<string, object>();

        public object Load(string key)
        {
            if (!settings.ContainsKey(key)) return null;
            else return settings[key];
        }

        public void Save(string key, object value)
        {
            if (settings.ContainsKey(key))
            {
                settings[key] = value;
            }
            else
            {
                settings.Add(key, value);
            }
        }
    }
}
