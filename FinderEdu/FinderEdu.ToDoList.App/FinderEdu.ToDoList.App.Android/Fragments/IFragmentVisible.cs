﻿namespace FinderEdu.ToDoList.App.Droid
{
    interface IFragmentVisible
    {
        void BecameVisible();
    }
}
