﻿using FinderEdu.ToDoList.App.Services;
using System;

namespace FinderEdu.ToDoList.App
{
    public class App
    {
        public static void Initialize()
        {
            //ServiceLocator.Instance.Register<IDataStore<Item>, MockDataStore>();
            ServiceLocator.Instance.Register<IDataStore<Item>, RemoteDataStore>();
            ServiceLocator.Instance.Register<IServer, Server>();
        }
    }
}
