﻿using FinderEdu.ToDoList.App.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace FinderEdu.ToDoList.App.Services
{

    public class Server : IServer
    {
        private HttpClient GetClient()
        {
            var client = new HttpClient()
            {
                BaseAddress = new Uri("http://localhost:55365/api/")
                //BaseAddress = new Uri("http://192.168.0.102:55365/api/")

            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }

        public async Task<List<Item>> LoadAllItemsAsync(string userId)
        {
            using (var client = GetClient())
            {
                var json = await client.GetStringAsync($"todoitems/{userId}");
                return json.FromJson<List<Item>>();
            }
        }

        public async Task AddOrUpdateItemAsync(string userId, Item item)
        {
            using (var client = GetClient())
            {
                var result = await client.PutAsync($"todoitems/{userId}", JsonContent.FromObject(item));
                result.EnsureSuccessStatusCode();
            }
        }

        public async Task DeleteItemAsync(string userId, string itemId)
        {
            using (var client = GetClient())
            {
                var result = await client.DeleteAsync($"todoitems/{userId}/{itemId}");
                result.EnsureSuccessStatusCode();
            }
        }
    }
}
