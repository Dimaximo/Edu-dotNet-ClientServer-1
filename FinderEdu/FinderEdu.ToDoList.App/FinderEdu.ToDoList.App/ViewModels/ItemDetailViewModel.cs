﻿using System;

namespace FinderEdu.ToDoList.App
{
    public class ItemDetailViewModel : BaseViewModel
    {
        public Item Item { get; set; }
        public ItemDetailViewModel(Item item = null)
        {
            if (item != null)
            {
                Title = item.Title;
                Item = item;
            }
        }
    }
}
